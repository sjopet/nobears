<?php

declare(strict_types=1);

namespace App\Tests\Modal;

use App\Modal\Coordinates;
use PHPUnit\Framework\TestCase;

class CoordinatesTest extends TestCase
{
    private const VALID_LATITUDE = '52.006683';
    private const VALID_LONGITUDE = '5.044206';

    public function testItCanCreatesValidCoordinates(): void
    {
        $coordinates = new Coordinates(self::VALID_LATITUDE, self::VALID_LONGITUDE);

        $this->assertSame(self::VALID_LATITUDE, $coordinates->getLatitude());
        $this->assertSame(self::VALID_LONGITUDE, $coordinates->getLongitude());
    }

    public function testItThrowsExceptionForEmptyLatitude(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Please provide a value for Latitude');

        new Coordinates('', self::VALID_LONGITUDE);
    }

    public function testItThrowsExceptionForEmptyLongitude(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Please provide a value for Longitude');

        new Coordinates(self::VALID_LATITUDE, '');
    }

    /**
     * @dataProvider invalidCoordinateProvider
     */
    public function testItThrowsExceptionForInvalidLatitude(string $latitude): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Please provide a valid Latitude value');

        new Coordinates($latitude, self::VALID_LONGITUDE);
    }

    /**
     * @dataProvider invalidCoordinateProvider
     */
    public function testItThrowsExceptionForInvalidLongitude(string $longitude): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Please provide a valid Longitude value');

        new Coordinates(self::VALID_LATITUDE, $longitude);
    }

    public function invalidCoordinateProvider(): array
    {
        return [
            ['abcd'],
            ['1234'],
            ['999.645'],
        ];
    }
}
