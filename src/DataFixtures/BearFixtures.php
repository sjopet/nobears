<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Bear;
use App\Modal\CsvBear;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Fusonic\CsvReader\CsvReader;
use Fusonic\CsvReader\Exceptions\CsvReaderException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class BearFixtures extends Fixture
{
    public function __construct(
        #[Autowire('%kernel.project_dir%/data')]
        private readonly string $dataDir,
        private readonly LoggerInterface $logger
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $reader = new CsvReader($this->dataDir . '/beren_locaties.csv');

        try {
            $iterator = $reader->readObjects(CsvBear::class);

            foreach ($iterator as $csvBear) {
                $bear = Bear::createFromCsvBear($csvBear);
                $manager->persist($bear);
            }

            $manager->flush();

        } catch(CsvReaderException $exception) {
            $this->logger->error('Caught error reading CSV file: ' . $exception->getMessage());
        }
    }
}
