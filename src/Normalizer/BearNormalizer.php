<?php

declare(strict_types=1);

namespace App\Normalizer;

use App\Entity\Bear;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class BearNormalizer implements NormalizerInterface
{
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        return [
            'id'        => $object->getId(),
            'name'      => $object->getName(),
            'city'      => $object->getCity(),
            'province'  => $object->getProvince(),
            'latitude'  => $object->getCoordinates()->getLatitude(),
            'longitude' => $object->getCoordinates()->getLongitude()
        ];
    }

    public function supportsNormalization(mixed $data, string $format = null): bool
    {
        return $data instanceof Bear && 'json' === $format;
    }

    public function getSupportedTypes(?string $format): array
    {
        return ['json' => true];
    }
}

