<?php

declare(strict_types=1);

namespace App\Entity;

use App\Modal\Coordinates;
use App\Modal\CsvBear;
use App\Repository\BearRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BearRepository::class)]
#[ORM\UniqueConstraint(
    name: 'lat_long_unique_idx',
    columns: ['latitude', 'longitude']
)]
class Bear
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\Column(length: 255)]
    private string $city;

    #[ORM\Column(length: 255)]
    private string $province;

    #[ORM\Column(length: 25)]
    private string $latitude;

    #[ORM\Column(length: 25)]
    private string $longitude;

    private function __construct(
        string $name,
        string $city,
        string $province,
        Coordinates $coordinates
    ) {
        $this->name      = $name;
        $this->city      = $city;
        $this->province  = $province;
        $this->latitude  = $coordinates->getLatitude();
        $this->longitude = $coordinates->getLongitude();
    }

    public static function createFromCsvBear(CsvBear $csvBear): self
    {
        return new self(
            $csvBear->name,
            $csvBear->city,
            $csvBear->province,
            new Coordinates($csvBear->latitude, $csvBear->longitude)
        );
    }

    public static function createFromRequestArray(array $parameters): self
    {
        self::validateRequestValues($parameters);

        return new self(
            $parameters['name'],
            $parameters['city'],
            $parameters['province'],
            new Coordinates($parameters['latitude'], $parameters['longitude'])
        );
    }

    public function updateFromRequestArray(array $parameters): void
    {
        self::validateRequestValues($parameters);

        $this->name      = $parameters['name'];
        $this->city      = $parameters['city'];
        $this->province  = $parameters['province'];
        $this->latitude  = $parameters['latitude'];
        $this->longitude = $parameters['longitude'];
    }

    private static function validateRequestValues(array $parameters): void
    {
        if (! \array_key_exists('name', $parameters) || '' === $parameters['name']) {
            throw new \InvalidArgumentException('Please provide a value for `name`');
        }

        if (! \array_key_exists('city', $parameters) || '' === $parameters['city']) {
            throw new \InvalidArgumentException('Please provide a value for `city`');
        }

        if (! \array_key_exists('province', $parameters) || '' === $parameters['province']) {
            throw new \InvalidArgumentException('Please provide a value for `province`');
        }

        if (! \array_key_exists('latitude', $parameters) || ! \array_key_exists('longitude', $parameters)) {
            throw new \InvalidArgumentException('Please provide a value for `latitude` and `longitude`');
        }

        new Coordinates($parameters['latitude'], $parameters['longitude']);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getProvince(): string
    {
        return $this->province;
    }

    public function getCoordinates(): Coordinates
    {
        return new Coordinates($this->latitude, $this->longitude);
    }
}
