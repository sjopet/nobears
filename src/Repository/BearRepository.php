<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Bear;
use App\Modal\Coordinates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Bear>
 *
 * @method Bear|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bear|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bear[]    findAll()
 * @method Bear[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BearRepository extends ServiceEntityRepository
{
    public const DEFAULT_DISTANCE = 25;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bear::class);
    }

    /**
     * @return Bear[]
     */
    public function findByCoordinates(Coordinates $coordinates, int $distance): array
    {
        $selectStatement = \sprintf(
            'st_distance_sphere(point(%s, %s), point(l.longitude, l.latitude)) AS distance',
            $coordinates->getLongitude(),
            $coordinates->getLatitude()
        );

        return $this->createQueryBuilder('l')
            ->select('l')
            ->addSelect($selectStatement)
            ->having('distance < :distance')
            ->setParameter('distance', $distance * 1000)
            ->orderBy('distance', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
