<?php

declare(strict_types=1);

namespace App\Modal;

class Coordinates
{
    public function __construct(private readonly string $latitude, private readonly string $longitude)
    {
        $this->validateLatitude($latitude);
        $this->validateLongitude($longitude);
    }

    public function validateLatitude(string $latitude): void
    {
        if ('' === $latitude) {
            throw new \InvalidArgumentException('Please provide a value for Latitude');
        }

        if (!\preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $latitude)) {
            throw new \InvalidArgumentException('Please provide a valid Latitude value');
        }
    }

    public function validateLongitude(string $longitude): void
    {
        if ('' === $longitude) {
            throw new \InvalidArgumentException('Please provide a value for Longitude');
        }

        if (!\preg_match('/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $longitude)) {
            throw new \InvalidArgumentException('Please provide a valid Longitude value');
        }
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }
}
