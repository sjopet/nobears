<?php

declare(strict_types=1);

namespace App\Modal;

use Fusonic\CsvReader\Attributes\IndexMapping;

class CsvBear
{
    #[IndexMapping(0)]
    public string $name;

    #[IndexMapping(1)]
    public string $city;

    #[IndexMapping(2)]
    public string $province;

    #[IndexMapping(3)]
    public string $latitude;

    #[IndexMapping(4)]
    public string $longitude;
}
