<?php

declare(strict_types=1);

namespace App\Controller;

use App\Modal\Coordinates;
use App\Repository\BearRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Component\RateLimiter\RateLimiterFactory;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

class ListBearsController extends AbstractController
{
    public function __construct(
        private readonly BearRepository $bearRepository,
        private readonly CacheInterface $cachePool,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route('/bears/list', name: 'app_bears_list')]
    public function listBearsAction(Request $request, RateLimiterFactory $bearsApiLimiter): JsonResponse
    {
        $limiter = $bearsApiLimiter->create($request->getClientIp());

        if (false === $limiter->consume(1)->isAccepted()) {
            throw new TooManyRequestsHttpException();
        }

        $latitude  = $request->get('latitude');
        $longitude = $request->get('longitude');
        $distance  = (int) $request->get('distance', BearRepository::DEFAULT_DISTANCE);

        if (null === $latitude && null === $longitude) {
            return $this->json($this->getUnfilteredBearList());
        }

        $bearList = $this->getBearListWithFilters($latitude, $longitude, $distance);

        return $this->json($bearList);
    }

    private function getUnfilteredBearList(): array
    {
        $bearListCacheItem = $this->cachePool->getItem('unfiltered');

        if ($bearListCacheItem->isHit()) {
            $this->logger->debug('Returning unfiltered bearList from cache');

            return $bearListCacheItem->get();
        }

        $bearList = $this->bearRepository->findAll();

        $bearListCacheItem->set($bearList);
        $this->cachePool->save($bearListCacheItem);

        $this->logger->debug('Returning unfiltered bearList from database');

        return $bearList;
    }

    private function getBearListWithFilters(string $latitude, string $longitude, int $distance): array
    {
        $cacheKey = \sprintf('%s-%s-%d', $latitude, $longitude, $distance);
        $bearListCacheItem = $this->cachePool->getItem($cacheKey);

        if ($bearListCacheItem->isHit()) {
            $this->logger->debug('Fetching Bears from cache for latitude: {latitude}, longitude: {longitude}, distance: {distance}', [
                'latitude'  => $latitude,
                'longitude' => $longitude,
                'distance'  => $distance,
            ]);

            return $bearListCacheItem->get();
        }

        $bearList = $this->bearRepository->findByCoordinates(
            new Coordinates($latitude, $longitude),
            $distance
        );

        $bearListCacheItem->set($bearList);
        $this->cachePool->save($bearListCacheItem);

        $this->logger->debug('Fetching Bears from database for latitude: {latitude}, longitude: {longitude}, distance: {distance}', [
            'latitude'  => $latitude,
            'longitude' => $longitude,
            'distance'  => $distance,
        ]);

        return $bearList;
    }
}
