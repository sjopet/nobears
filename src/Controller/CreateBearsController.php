<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Bear;
use App\Repository\BearRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CreateBearsController extends AbstractController
{
    public function __construct(
        private readonly BearRepository $bearRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route('/bears/create', name: 'app_bears_create')]
    public function createBearsAction(Request $request): JsonResponse
    {
        $name      = $request->get('name');
        $city      = $request->get('city');
        $province  = $request->get('province');
        $latitude  = $request->get('latitude');
        $longitude = $request->get('longitude');

        try {
            $bear = Bear::createFromRequestArray([
                'name'      => $name,
                'city'      => $city,
                'province'  => $province,
                'latitude'  => $latitude,
                'longitude' => $longitude,
            ]);
        } catch (\InvalidArgumentException $exception) {
            return $this->json(['error' => $exception->getMessage()]);
        }

        $existingBear = $this->bearRepository->findOneBy(['latitude' => $latitude, 'longitude' => $longitude]);

        if ($existingBear instanceof Bear) {
            return $this->json([
                'error' => \sprintf('A Bear with coordinates `%s / %s` already exists.', $latitude, $longitude)
            ]);
        }

        $this->logger->debug('Creating new Bear `{name}`', [
            'name' => $name,
        ]);

        $this->entityManager->persist($bear);
        $this->entityManager->flush();

        return $this->json(['message' => 'The Bear was added successfully.']);
    }
}
