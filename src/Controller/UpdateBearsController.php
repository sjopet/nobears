<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Bear;
use App\Repository\BearRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UpdateBearsController extends AbstractController
{
    public function __construct(
        private readonly BearRepository $bearRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route('/bears/update/{id}', name: 'app_bears_update')]
    public function updateBearsAction(Request $request, Bear $bear): JsonResponse
    {
        $name      = $request->get('name');
        $city      = $request->get('city');
        $province  = $request->get('province');
        $latitude  = $request->get('latitude');
        $longitude = $request->get('longitude');

        try {
            $bear->updateFromRequestArray([
                'name'      => $name,
                'city'      => $city,
                'province'  => $province,
                'latitude'  => $latitude,
                'longitude' => $longitude,
            ]);
        } catch (\InvalidArgumentException $exception) {
            return $this->json(['error' => $exception->getMessage()]);
        }

        $existingBear = $this->bearRepository->findOneBy(['latitude' => $latitude, 'longitude' => $longitude]);

        if ($existingBear instanceof Bear) {
            return $this->json([
                'error' => \sprintf('A Bear with coordinates `%s / %s` already exists.', $latitude, $longitude)
            ]);
        }

        $this->logger->debug('Updating Bear `{name} ({id})`', [
            'name' => $bear->getName(),
            'id'   => $bear->getId(),
        ]);

        $this->entityManager->flush();

        return $this->json(['message' => 'The Bear was updated successfully.']);
    }
}
