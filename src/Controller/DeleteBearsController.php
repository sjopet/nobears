<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Bear;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DeleteBearsController extends AbstractController
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger
    ) {
    }

    #[Route('/bears/delete/{id}', name: 'app_bears_delete')]
    public function deleteBearsAction(Request $request, Bear $bear): JsonResponse
    {
        $this->logger->debug('Deleting Bear `{name} ({id})`', [
            'name' => $bear->getName(),
            'id'   => $bear->getId(),
        ]);

        $this->entityManager->remove($bear);
        $this->entityManager->flush();

        return $this->json(['message' => 'The Bear was deleted successfully.']);
    }
}
